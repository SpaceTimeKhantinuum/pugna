#!/usr/bin/env python
import os

os.environ['CUDA_VISIBLE_DEVICES'] = '0,1,2,3,4'

# https://keras.io/guides/distributed_training/

import tensorflow as tf
from tensorflow import keras
import numpy as np
import os
from os.path import join

import pugna.models.mscalednn

def load_numpy_into_tf_dataset(xfile, yfile, batch_size):
    """
    batch_size: global batch size?
    """
    x = np.load(xfile)
    y = np.load(yfile)

    return tf.data.Dataset.from_tensor_slices((x, y)).batch(batch_size)

def get_compiled_model(input_dim, output_dim, nlayes, units, nscales, activations, dropouts, batch_norms, scale_names, learning_rate, adam_amsgrad):
    model = pugna.models.mscalednn.build_model(
        input_dim, output_dim, nlayers, units, nscales, activations, dropouts, batch_norms, scale_names)
    opt = tf.keras.optimizers.Adam(
        learning_rate=learning_rate, amsgrad=adam_amsgrad)
    loss = 'mse'
    model.compile(loss=loss, optimizer=opt)
    return model


if __name__ == "__main__":


    # gpu_devices = '0,1,3,4,7'
    gpu_devices = '0'
    os.environ['CUDA_VISIBLE_DEVICES'] = gpu_devices
    num_gpus = len(os.environ['CUDA_VISIBLE_DEVICES'].split(','))
    print(f"num_gpus: {num_gpus}")

    # mirrored_strategy = tf.distribute.MirroredStrategy(devices=["/gpu:7", "/gpu:6"])
    # Create a MirroredStrategy.
    strategy = tf.distribute.MirroredStrategy()
    print("Number of devices: {}".format(strategy.num_replicas_in_sync))


    gpus = tf.config.experimental.list_physical_devices('GPU')
    #gpus = tf.config.experimental.list_physical_devices('XLA_GPU')
    for gpu in gpus:
        print("Name:", gpu.name, "  Type:", gpu.device_type)

    # max_samples = 1000000
    max_samples = 9306300
    # global_batch_size = 512
    # local_batch_size = int(global_batch_size / num_gpus)
    local_batch_size = 512
    # local_batch_size = 1024
    # local_batch_size = 100000
    global_batch_size = local_batch_size * num_gpus

    print(f"global_batch_size: {global_batch_size}")
    print(f"local_batch_size: {local_batch_size}")

    root_dir = "/home/sebastian.khan/data/test-3d-prec-q2-short/smaller-datasets"

    x_train_file = join(root_dir, f"{max_samples}/train/MinMaxScaler/phase_X_train_scaled.npy")
    y_train_file = join(root_dir, f"{max_samples}/train/MinMaxScaler/phase_y_train_scaled.npy")
    x_val_file = join(root_dir, f"{max_samples}/val/MinMaxScaler/phase_X_val_scaled.npy")
    y_val_file = join(root_dir, f"{max_samples}/val/MinMaxScaler/phase_y_val_scaled.npy")

    train_dataset = load_numpy_into_tf_dataset(x_train_file, y_train_file, global_batch_size).cache().prefetch(tf.data.experimental.AUTOTUNE)
    val_dataset = load_numpy_into_tf_dataset(x_val_file, y_val_file, global_batch_size).cache().prefetch(tf.data.experimental.AUTOTUNE)

    # this is just like x.shape[1] and y.shape[1]
    input_dim = train_dataset.element_spec[0].shape[1]
    output_dim = train_dataset.element_spec[1].shape[1]
    nlayers=4
    units=[320, 320, 320, 320]
    nscales=[9,1,1,1]
    activations=['s2relu','s2relu','s2relu','s2relu']
    dropouts=[0,0,0,0]
    batch_norms=[False, False, False, False]
    scale_names=['base2','linear','linear','linear']
    learning_rate=0.001
    adam_amsgrad=True



    # Open a strategy scope.
    with strategy.scope():
        # Everything that creates variables should be under the strategy scope.
        # In general this is only model construction & `compile()`.
        model = get_compiled_model(input_dim, output_dim, nlayers, units, nscales, activations, dropouts, batch_norms, scale_names, learning_rate, adam_amsgrad)


    print("fitting")
    model.fit(train_dataset, epochs=5, validation_data=val_dataset)
    print('evaluating')
    model.evaluate(train_dataset)
