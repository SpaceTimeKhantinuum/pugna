import os

os.environ['CUDA_VISIBLE_DEVICES'] = '5,6,7'

import tensorflow as tf
mirrored_strategy = tf.distribute.MirroredStrategy(devices=["/gpu:0", "/gpu:1"])
