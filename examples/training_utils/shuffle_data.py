import numpy as np
from pugna import training_utils

xtest = np.random.uniform(0, 1, size=(5, 3))
ytest = np.arange(xtest.shape[0])

print(xtest)
print(ytest)

xtest2, ytest2 = training_utils.shuffle_data(xtest, ytest, size=3)

print(xtest2)
print(ytest2)
