import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split

x = np.linspace(-1, 1, 10000)[:, np.newaxis]

y = np.sin(23*x) + np.sin(137*x) + np.sin(203*x)


plt.figure(figsize=(14, 4))
plt.plot(x, y)
plt.savefig('data0.png')
plt.close()

X_train, X_test, y_train, y_test = train_test_split(
    x, y, test_size=0.33, random_state=42)

print(X_train.shape, X_test.shape, y_train.shape, y_test.shape)

plt.figure(figsize=(14, 4))
plt.plot(X_train, y_train, 'x', label='train')
plt.plot(X_test, y_test, 'o', label='test')
plt.legend()
plt.savefig('data.png')
plt.close()


np.save("x.npy", X_train)
np.save("y.npy", y_train)
np.save("x_val.npy", X_test)
np.save("y_val.npy", y_test)
