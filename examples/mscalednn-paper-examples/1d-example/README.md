# Reproducing Examples from MscaleDNN paper

paper: https://arxiv.org/abs/1910.11710

## 1D example

See page 8.

function

```math
f(x) = sin(23x) + sin(137x) + sin(203x) \quad x \in [0, \pi]
```
