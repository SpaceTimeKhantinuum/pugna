#!/bin/bash

#LRS="ReduceLROnPlateau"
LRS="CosineDecayRestarts"

#NSCALES=1
#NSCALES=10
#NSCALES=50
#NSCALES=100

pugna_fit \
-v \
--output-dir bias-init-10-10-more-scales-test-ms_less_data_3layer \
--X-data-train x.npy \
--y-data-train y.npy \
--X-data-val x_val.npy \
--y-data-val y_val.npy \
--nlayers 2 \
--units 2048 128 \
--nscales 2048 1 \
--activations s2relu \
--dropouts 0 \
--batch-norms False \
--epochs 10000 \
--learning-rate 0.001 \
--lrs-name ${LRS} \
--lrs-cosine-first-decay-steps 2 \
--adam-amsgrad \
--scale-names linear \
--mscale-bias-init -2 2
