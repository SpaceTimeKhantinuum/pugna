import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split

x = np.linspace(-1, 1, 500)[:,np.newaxis]
y = x**2

X_train, X_test, y_train, y_test = train_test_split(x, y, test_size=0.33, random_state=42)

print(X_train.shape, X_test.shape, y_train.shape, y_test.shape)

np.save("x.npy", X_train)
np.save("y.npy", y_train)
np.save("x_val.npy", X_test)
np.save("y_val.npy", y_test)
