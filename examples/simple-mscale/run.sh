#!/bin/bash

# LRS="ReduceLROnPlateau"
LRS="CosineDecayRestarts"

# scale-name: base2
#pugna_fit \
#-v \
#--output-dir results_nscale10_$LRS \
#--X-data-train x.npy \
#--y-data-train y.npy \
#--X-data-val x_val.npy \
#--y-data-val y_val.npy \
#--nlayers 2 \
#--units 1000 \
#--nscales 4 \
#--activations s2relu \
#--dropouts 0 \
#--batch-norms False \
#--epochs 500 \
#--learning-rate 0.001 \
#--lrs-name ${LRS} \
#--lrs-cosine-first-decay-steps 2 \
#--adam-amsgrad \
#--scale-names base2

# scale-name: linear
pugna_fit \
-v \
--output-dir results_nscale10_$LRS \
--X-data-train x.npy \
--y-data-train y.npy \
--X-data-val x_val.npy \
--y-data-val y_val.npy \
--nlayers 2 \
--units 1000 \
--nscales 10 \
--activations s2relu \
--dropouts 0 \
--batch-norms False \
--epochs 500 \
--learning-rate 0.001 \
--lrs-name ${LRS} \
--lrs-cosine-first-decay-steps 2 \
--adam-amsgrad \
--scale-names linear 
#\
#--tensorboard-dir ./logs
