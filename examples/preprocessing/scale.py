import numpy as np
import pugna.data

v1 = np.random.uniform(4, 25, 10000)
v2 = np.random.uniform(-10, 25, 10000)
v3 = np.random.normal(10, 2, 10000)

# column stack features
# so that the shape is (N,M)
# where N is the number of samples/entires
# and M is the number of features/dimensions
X = np.column_stack((v1, v2, v3))

print(f"X.shape: {X.shape}")

print("")

method = "StandardScaler"
print(f"using method = {method}")

X_scalers = pugna.data.make_scalers(X, method=method)
X_scaled = pugna.data.apply_scaler(X, X_scalers)

for i in range(X_scaled.shape[1]):
    print(f"feature: {i}")
    print("\tmin: ", X_scaled[:, i].min())
    print("\tmax: ", X_scaled[:, i].max())
    print("\tmean: ", X_scaled[:, i].mean())
    print("\tstd: ", np.std(X_scaled[:, i]))


print("")

method = "MinMaxScaler"
print(f"using method = {method}")

X_scalers = pugna.data.make_scalers(X, method=method, feature_range=(-1,1))
X_scaled = pugna.data.apply_scaler(X, X_scalers)

for i in range(X_scaled.shape[1]):
    print(f"feature: {i}")
    print("\tmin: ", X_scaled[:, i].min())
    print("\tmax: ", X_scaled[:, i].max())
    print("\tmean: ", X_scaled[:, i].mean())
    print("\tstd: ", np.std(X_scaled[:, i]))


print("")

print("storing test data")

test_data = np.zeros((3, 4))

for i in range(X_scaled.shape[1]):
    test_data[i][0] = X_scaled[:, i].min()
    test_data[i][1] = X_scaled[:, i].max()
    test_data[i][2] = X_scaled[:, i].mean()
    test_data[i][3] = np.std(X_scaled[:, i])

print("saving scalers")

pugna.data.save_scalers(X_scalers, "X_scalers.npy")

print("loading scalers")

X_scalers = pugna.data.load_scalers("X_scalers.npy")

X_scaled = pugna.data.apply_scaler(X, X_scalers)

print("testing for equality")

for i in range(X_scaled.shape[1]):
    assert X_scaled[:, i].min() == test_data[i][0], "min failed"
    assert X_scaled[:, i].max() == test_data[i][1], "max failed"
    assert X_scaled[:, i].mean() == test_data[i][2], "mean failed"
    assert np.std(X_scaled[:, i]) == test_data[i][3], "std failed"

print("test passes!")
