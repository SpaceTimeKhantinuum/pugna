from tensorflow.keras.layers import LeakyReLU
from tensorflow.keras.layers import Dense
from tensorflow.keras.models import Sequential
import glob
import matplotlib.pyplot as plt
import matplotlib
import time

import pugna.trainer
import pugna.callbacks

import numpy as np
import tensorflow as tf

matplotlib.rcParams.update({"font.size": 16})

ADD_NOISE = False


def gen_data(npts=1000):
    x = np.linspace(0, 1, npts)
    y = x + x ** 2 * np.sin(x * 2 * np.pi / 0.5 / 0.5)
    return x.reshape(-1, 1), y.reshape(-1, 1)


def add_noise(y, seed=1):
    y = y.copy()
    np.random.seed(seed)
    return y + np.random.normal(loc=y, scale=0.05, size=y.shape)


# X, y = gen_data(npts=10000)
# X, y = gen_data(npts=1000)
# X, y = gen_data(npts=100)
X, y = gen_data(npts=500)

if ADD_NOISE:
    y = add_noise(y)

Xval, yval = gen_data(npts=100)
if ADD_NOISE:
    yval = add_noise(yval)

print(X.shape)
print(y.shape)

print(Xval.shape)
print(yval.shape)

plt.figure()
plt.plot(X, y, "k.-")
plt.title("the data")
plt.show()
plt.close()

plt.figure()
plt.plot(Xval, yval, "C0.-")
plt.title("the validation data")
plt.show()
plt.close()

trainer = pugna.trainer.IterativeTraining()


def create_model():
    dim = X.shape[1]
    model = tf.keras.models.Sequential()
    model.add(tf.keras.Input(shape=dim))
    units = [64, 64, 64, 64, 64, 64]
    for unit in units:
        model.add(tf.keras.layers.Dense(unit, activation='relu'))
    model.add(tf.keras.layers.Dense(1))
    return model


trainer.set_build_model_func(create_model)

strategy = tf.distribute.MirroredStrategy()
trainer.set_strategy(strategy)
print('Number of devices: {}'.format(strategy.num_replicas_in_sync))

# trainer.set_model(model=model)
trainer.set_callbacks(callbacks=[pugna.callbacks.PrintDot()])
trainer.set_optimiser(tf.keras.optimizers.Adam())

trainer.initialise(2, epochs=[100, 100], learning_rates=[1e-3, 1e-3/2])

print(trainer.info)

trainer.train(X, y, verbose=True, validation_data=(Xval, yval))

fig = trainer.plot_loss()
ax = fig.gca()
ax.axhline(0.013302860781550407)
plt.tight_layout()
plt.show()
plt.close()

plt.figure()
plt.plot(X, y)
plt.plot(X, trainer.model.predict(X), ls='--')
plt.show()
plt.close()

plt.figure()
plt.plot(X, y-trainer.model.predict(X))
plt.show()
plt.close()

plt.figure()
plt.plot(X, np.abs(y-trainer.model.predict(X)))
plt.yscale('log')
plt.show()
plt.close()
