import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
import pugna.learning_rate_schedulers

initial_learning_rate = 1e-3
decay_steps = int(100.0)
decay_rate = 10.

# tf inverse time decay
learning_rate_fn_inv_time = tf.keras.optimizers.schedules.InverseTimeDecay(
    initial_learning_rate, decay_steps, decay_rate, staircase=True)


# my modified inverse time decay with a final_learning_rate
final_learning_rate = 1e-5
learning_rate_fn_inv_time_with_final_LR = pugna.learning_rate_schedulers.InverseTimeDecay_WithFinalLR(
    initial_learning_rate, final_learning_rate, decay_steps, decay_rate, staircase=True)


# my modified version with a very low final learning rate to show that this is the
# same as the original tf.keras.optimizers.schedules.InverseTimeDecay function.
final_learning_rate = 1e-10
learning_rate_fn_inv_time_with_final_LR_comparison = pugna.learning_rate_schedulers.InverseTimeDecay_WithFinalLR(
    initial_learning_rate, final_learning_rate, decay_steps, decay_rate, staircase=True)

epochs = np.arange(10000)

y1 = learning_rate_fn_inv_time_with_final_LR(epochs).numpy()
y2 = learning_rate_fn_inv_time(epochs).numpy()
y3 = learning_rate_fn_inv_time_with_final_LR_comparison(epochs).numpy()

plt.figure()
plt.plot(y1, label='Inv.Time with final LR')
plt.plot(y2, ls='--', label='Inv.Time (original)')
plt.plot(y3, ls='-.', label='Inv.Time with very low final LR')
plt.yscale('log')
plt.legend()
plt.show()
plt.close()
